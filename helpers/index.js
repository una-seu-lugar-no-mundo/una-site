module.exports = {
    getRangeIdToPopulation: (age) => {
        var rangeId = 60
        for (var index = 1; index <= 17; index++) {
            var limit = (5 * index) - 1
            if (age <= limit) {
                rangeId = rangeId + index
                break;
            }   
        }
        return rangeId 
    },
    getLocationId: (text) => {
        var idForUF = {
            'AC':12,
            'AL':27,
            'AP':16,
            'AM':13,
            'BA':29,
            'CE': 23,
            'DF':53,
            'ES':32,
            'GO':52,
            'MA':21,
            'MT':51,
            'MS':50,
            'MG':31,
            'PA':15,
            'PB':25,
            'PR':41,
            'PE':26,
            'PI':22,
            'RJ':33,
            'RN':24,
            'RS':43,
            'RO':11,
            'RR':14,
            'SC':42,
            'SP':35,
            'SE':28,
            'TO':17
        }
        var id = idForUF[text.toUpperCase()]
        return id
    },
    getGenderId: (text) => {
        var id = 1
        if (text.toLowerCase() === "mulher") {
            id = 2
        } 
        return id
    },
    getRangeIdToIlliteracy: (age) => {
        var rangeId = 0

        if (age <= 29) {
            rangeId = 40
        } else if (age <= 59) {
            rangeId = 41
        } else {
            rangeId = 8
        }
        return rangeId
    },
    getRangeIdToRevenue: (age) => {
        var rangeId = 0

        if (age <= 24) {
            rangeId = 82
        } else if (age <= 39) {
            rangeId = 83
        } else if (age <= 59) {
            rangeId = 84
        } else {
            rangeId = 8
        }
        return rangeId
    }
}