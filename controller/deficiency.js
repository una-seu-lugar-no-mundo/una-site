const curl = require('curl');
const helpers = require('../helpers')

var getDeficiencyData = (gender, location, age, callback) => {
    var genderId = helpers.getGenderId(gender)
    var locationId = helpers.getLocationId(location)

    var json = {}
    fetchBlindData(genderId, locationId, function(result) {
        json["Visual"] = result
        fetchMotorData(genderId, locationId, function(result) {
            json["Motora"] = result
            fetchMentalData(genderId, locationId, function(result) {
                json["Intelectual"] = result
                fetchSoundData(genderId, locationId, function(result) {
                    json["Auditiva"] = result
                    callback(json)
                })
            })
        })
    })
}

var fetchBlindData = (genderId, locationId, callback) => {
    var url = "http://servicodados.ibge.gov.br/api/v1/snig?resp=val&ind=4642&lc=" + locationId + "-" + genderId + ",128"
    curl.get(url, function(err, response, body) {
        var data = JSON.parse(body)
        callback(data[0]["v"])
    })
}

var fetchMotorData = (genderId, locationId, callback) => {
    var url = "http://servicodados.ibge.gov.br/api/v1/snig?resp=val&ind=4644&lc=" + locationId + "-" + genderId + ",128"
    curl.get(url, function(err, response, body) {
        var data = JSON.parse(body)
        callback(data[0]["v"])
    })
}

var fetchMentalData = (genderId, locationId, callback) => {
    var url = "http://servicodados.ibge.gov.br/api/v1/snig?resp=val&ind=4645&lc=" + locationId + "-" + genderId + ",128"
    curl.get(url, function(err, response, body) {
        var data = JSON.parse(body)
        callback(data[0]["v"])
    })
}

var fetchSoundData = (genderId, locationId, callback) => {
    var url = "http://servicodados.ibge.gov.br/api/v1/snig?resp=val&ind=4643&lc=" + locationId + "-" + genderId + ",128"
    curl.get(url, function(err, response, body) {
        var data = JSON.parse(body)
        callback(data[0]["v"])
    })
}

module.exports = getDeficiencyData
