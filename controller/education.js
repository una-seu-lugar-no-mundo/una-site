const curl = require('curl');
const helpers = require('../helpers')

var getEducationData = (gender, location, age, callback) => {
    var genderId = helpers.getGenderId(gender)
    var locationId = helpers.getLocationId(location)

    var json = {}
    fetchEvasionData(genderId, locationId, function(result) {
        json["Abandono"] = result
        fetchIlliteracyData(genderId, locationId, age, function(result){
            json["Analfabetismo"] = result
            fetchLevelData(genderId, locationId, function(result) {
                json["Nivel"] = result
                callback(json)
            })
        })
    })
}

var fetchEvasionData = (genderId, locationId, callback) => {
    var url = "http://servicodados.ibge.gov.br/api/v1/snig?resp=val&ind=4700&lc=" + locationId + "-" + genderId + ",128"
    curl.get(url, function(err, response, body) {
        var data = JSON.parse(body)
        callback(data[0]["v"])
    })
}

var fetchIlliteracyData = (genderId, locationId, age, callback) => {
    var rangeId = helpers.getRangeIdToIlliteracy(age)
    var url = "http://servicodados.ibge.gov.br/api/v1/snig?resp=val&ind=4693&lc=" + locationId + "-" + genderId + "," + rangeId + ",128"
    curl.get(url, function(err, response, body) {
        var data = JSON.parse(body)
        callback(data[0]["v"])
    })
}

var fetchLevelData = (genderId, locationId, callback) => {
    var url = "http://servicodados.ibge.gov.br/api/v1/snig?resp=val&ind=4699&lc=" + locationId + "-"+ genderId + ",47,128:" + genderId + ",48,128:" + genderId + ",45,128:" + genderId + ",46,128"
    curl.get(url, function(err, response, body) {
        var json = {}
        var data = JSON.parse(body)
        data.forEach(function(element) {
            var items = element["c"].split(",")
            var cod = items[1]
            if (cod == 45) {
                json["Nenhum"] = element["v"]
            }
            if (cod == 46) {
                json["Fundamental"] = element["v"]
            } 
            if (cod == 47) {
                json["Médio"] = element["v"]
            }
            if (cod == 48) {
                json["Superior"] = element["v"]
            }
        })
        callback(json)
    })
}

module.exports = getEducationData