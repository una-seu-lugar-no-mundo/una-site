const curl = require('curl');
const helpers = require('../helpers')

var getGenderData = (gender, location, age, callback) => {
    var genderId = helpers.getGenderId(gender)
    var locationId = helpers.getLocationId(location)

    var json = {}
    fetchServicesData(genderId, locationId, function(result) {
        json["Serviços"] = result
        fetchRevenuesData(age, locationId, function(result){
            json["Rendimentos"] = result
            fetchPopulationData(age, locationId, function(result) {
                json["Populacao"] = result
                callback(json)
            })
        })
    })

}

var fetchServicesData = (genderId, locationId, callback) => {
    var url = "http://servicodados.ibge.gov.br/api/v1/snig?resp=val&ind=4741&lc="+ locationId + "-" + genderId + ",112,128:" + genderId + ",113,128:" + genderId + ",114,128"
    curl.get(url, function(err, response, body) {
        var json = {}
        var data = JSON.parse(body)
        data.forEach(function(element) {
            var items = element["c"].split(",")
            var cod = items[1]
            if (cod == 112) {
                json["Agricultura"] = element["v"]
            }
            if (cod == 113) {
                json["Industria"] = element["v"]
            } 
            if (cod == 114) {
                json["Serviços"] = element["v"]
            }
        })
        callback(json)
    })
}

var fetchRevenuesData = (age, locationId, callback) => {
    var rangeId = helpers.getRangeIdToRevenue(age)
    var url = "http://servicodados.ibge.gov.br/api/v1/snig?resp=val&ind=4721&lc=" + locationId + "-" + rangeId + ",128"
    curl.get(url, function(err, response, body) {
        var data = JSON.parse(body)
        callback(data[0]["v"])
    })
}

var fetchPopulationData = (age, locationId, callback) => {
    var rangeId = helpers.getRangeIdToPopulation(age)
    var url = "http://servicodados.ibge.gov.br/api/v1/snig?resp=val&ind=4707&lc=" + locationId + "-" + rangeId + ",128"
    curl.get(url, function(err, response, body) {
        var data = JSON.parse(body)
        callback(data[0]["v"])
    })
}

module.exports = getGenderData