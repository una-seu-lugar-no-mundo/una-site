const express = require('express');
const router = express.Router();
const curl = require('curl');
const genderController = require('../controller/gender')
const educationController = require('../controller/education')
const deficiencyController = require('../controller/deficiency')

/* GET home page. */
router.get('/estatisticaGenero', function(req, res, next) {
  const endPoint = "http://servicodados.ibge.gov.br/api/v1/pesquisas/11/periodos/all/indicadores/6.4/resultados/'53'";
  curl.get(endPoint, function(err, response, body) {
    res.setHeader('Content-Type', 'application/json');
    res.send(body);
  });
});

router.get('/populacaoPorEstado/', function(req, res, next) {
  let state = req.query.state;
  const endPoint = "http://www.ibge.gov.br/apps/populacao/projecao/jsdados/"+ state +"_faixas.js";
  curl.get(endPoint, function(err, response, body) {
    res.setHeader('Content-Type', 'application/json');
    res.send(body);
  });
});


// dados em porcentagem
router.get('/feminicidioPorRegiao', function(req, res, next) {
  res.setHeader('Content-Type', 'application/json');
  res.send('{"brasil":21.0,"centro-oeste":43.2,"sul":25.8,"sudeste":-22.5,"nordeste":93.7,"norte":112.2}');
});

router.get('/nome', function(req, res, next) {
  let name = req.query.name;
  const endPoint = "http://servicodados.ibge.gov.br/api/v1/censos/nomes/basica?nome=" + name;
  curl.get(endPoint, function(err, response, body) {
    res.setHeader('Content-Type', 'application/json');
    res.send(body);
  });
});

router.get('/getGenderData', function(req, res, next) {
    var gender = req.query.gender
    var location = req.query.location
    var age = req.query.age

    genderController(gender, location, age, function(result) {
      res.send(result)
    })
});

router.get('/getEducationData', function(req, res, next) {
    var gender = req.query.gender
    var location = req.query.location
    var age = req.query.age
    
    educationController(gender, location, age, function(result) {
      res.send(result)
    })
});

router.get('/getDeficiencyData', function(req, res, next) {
    var gender = req.query.gender
    var location = req.query.location
    var age = req.query.age
    
    deficiencyController(gender, location, age, function(result) {
      res.send(result)
    })
});

module.exports = router;
